package es.uniovi.benja.partidosgw2;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class partidoDetalles_actividad extends ActionBarActivity {

    ExpandableListView expListView;
    List<String> groupList;
    List<String> childList;
    Map<String, List<String>> matchCollection;
    private List<PartidoInfoMapa> objetivos;
    private String partidoID;
    private PartidosCountDownTimer countDownTimer;
    private long startTime = Long.MAX_VALUE;
    private long segundos = 0;
    private Button button;
    private boolean hayConexion;


    // Detección de la conexión: Sesión nº10 de Prácticas de Laboratorio
    boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partido_detalles);

        recuperarDatos(false);


//        NumberPicker np = (NumberPicker) findViewById(R.id.tiempo_refresco);
//        String[] nums = new String[20];
//        for(int i=0; i<nums.length; i++)
//            nums[i] = Integer.toString(i);

//        np.setMinValue(1);
//        np.setMaxValue(20);
//        np.setWrapSelectorWheel(false);
//        np.setDisplayedValues(nums);
//        np.setValue(1);


    }

    // Clase CountDownTimer
    public class PartidosCountDownTimer extends CountDownTimer
    {

        public PartidosCountDownTimer(long startTime, long interval)
        {
            super(startTime, interval);
        }

        @Override
        public void onFinish()
        {
//            text.setText("Time's up!");
//            timeElapsedView.setText("Time Elapsed: " + String.valueOf(startTime));
            Toast.makeText(getBaseContext(), "La actualización ha terminado :)", Toast.LENGTH_LONG)
                    .show();
        }

        @Override
        public void onTick(long millisUntilFinished)
        {
//            text.setText("Time remain:" + millisUntilFinished);
//            timeElapsed = startTime - millisUntilFinished;
//            timeElapsedView.setText("Time Elapsed: " + String.valueOf(timeElapsed));
            recuperarDatos(true);

        }
    }

    public void recuperarDatos(boolean refrescar){

        // El siguiente código es lo que la tarea debe hacer:
        Intent intent = getIntent();
        partidoID = getIntent().getStringExtra(ExpandableListAdapter.EXTRA_MESSAGE);
        objetivos = new ArrayList<PartidoInfoMapa>();
        this.cargarObjetivos();
        JSONParserPartidosD json = new JSONParserPartidosD(this, partidoDetalles_actividad.this, refrescar);
        estructurarLista();


        hayConexion = isOnline();

        if (hayConexion){
            try {
                json.recuperarDetalles(partidoID);
            } catch (Exception e){
                e.getStackTrace();
            }
        } else {

            String fallo_conexion = getResources().getString(R.string.fallo_conexion);
            Toast.makeText(getBaseContext(),
                    fallo_conexion,
                    Toast.LENGTH_LONG)
                    .show();
        }



    }

    public void expandable(){
        expListView = (ExpandableListView) findViewById(R.id.expandableListView);
        final ExpandableListAdapterObjetivos expListAdapter = new ExpandableListAdapterObjetivos(
                this, groupList, matchCollection);


        LayoutInflater inflater=this.getLayoutInflater();

        View header = inflater.inflate(R.layout.layoutaux, null, false);
        expListView.addHeaderView(header);
        expListView.setAdapter(expListAdapter);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                final String selected = (String) expListAdapter.getChild(
                        groupPosition, childPosition);
                Toast.makeText(getBaseContext(), selected, Toast.LENGTH_LONG)
                        .show();

                return true;
            }
        });
    }

    public void createGroupList(){
        String idioma = Locale.getDefault().getDisplayLanguage();
        groupList = new ArrayList<String>();
        if (idioma.equals("English")){
            groupList.add("Red home objectives");
            groupList.add("Blue home objectives");
            groupList.add("Green home objectives");
            groupList.add("Center home objectives");
        } else if (idioma.equals("español")){
            groupList.add("Objetivos hogar Rojo");
            groupList.add("Objetivos hogar Azul");
            groupList.add("Objetivos hogar Verde");
            groupList.add("Objetivos hogar Central");
        }
    }

    public void cargarObjetivos (){
        PartidoInfoMapa partidoInfo = new PartidoInfoMapa(1, "Overlook", 25);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(2, "Valley", 25);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(3, "Lowlands", 25);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(4, "Golanta Clearing", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(5, "Pangloss Rise", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(6, "Speldan Clearcut", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(7, "Danelon Passage", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(8, "Umberglade Woods", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(9, "Stonemist Castle", 35);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(10, "Rogue's Quarry", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(11, "Aldon's Ledge", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(12, "Wildcreek Run", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(13, "Jerrifer's Slough", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(14, "Klovan Gully", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(15, "Langor Gulch", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(16, "Quentin Lake", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(17, "Mendon's Gap", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(18, "Anzalias Pass", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(19, "Ogrewatch Cut", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(20, "Veloka Slope", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(21, "Durios Gulch", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(22, "Bravost Escarpment", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(23, "Garrison", 25);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(24, "Champion's demense", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(25, "Redbriar", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(26, "Greenlake", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(27, "Ascension Bay", 25);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(28, "Dawn's Eyrie", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(29, "The Spiritholme", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(30, "Woodhaven", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(31, "Askalion Hills", 25);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(32, "Etheron Hills", 25);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(33, "Dreaming Bay", 25);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(34, "Victor's Lodge", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(35, "Greenbriar", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(36, "Bluelake", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(37, "Garrison", 25);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(38, "Longview", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(39, "The Godsword", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(40, "Cliffside", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(41, "Shadaran Hills", 25);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(42, "Redlake", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(43, "Hero's Lodge", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(44, "Dreadfall Bay", 25);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(45, "Bluebriar", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(46, "Garrison", 25);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(47, "Sunnyhill", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(48, "Faithleap", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(49, "Bluevale Refuge", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(50, "Bluewater Lowlands", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(51, "Astralholme", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(52, "Arah's Hope", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(53, "Greenvale Refuge", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(54, "Foghaven", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(55, "Redwater Lowlands", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(56, "The Titanpaw", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(57, "Cragtop", 10);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(58, "Godslore", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(59, "Redvale Refuge", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(60, "Stargrove", 5);
        objetivos.add(partidoInfo);
        partidoInfo = new PartidoInfoMapa(61, "Greenwater Lowlands", 5);
        objetivos.add(partidoInfo);
    }

    public String getNombreEquipo(String owner){
        String nombreEquipo = "";
        if (owner.equals("Red")){
            nombreEquipo = "Rojo";
        }else if (owner.equals("Blue")){
            nombreEquipo = "Azul";
        } else if (owner.equals("Green")){
            nombreEquipo = "Verde";
        } else if (owner.equals("Neutral")){
            nombreEquipo = "Neutral";
        }
        return nombreEquipo;
    }

    public String getNombreZona(String id){
        int id_num = Integer.parseInt(id);
        String nombreZona = "";
        for (int i = 0; i < objetivos.size(); i++){
            if (objetivos.get(i).getId() == id_num){
                nombreZona = objetivos.get(i).getNombre();
            }
        }
        return nombreZona;
    }

    public void createCollection(List<PartidoObjetivos> listObjetivos){

        /*
        String[] objetivos = {"Objetivo1","Objetivo2","Objetivo3","Objetivo4"};

        matchCollection = new LinkedHashMap<String, List<String>>();
        for (String objetivo : groupList){
            if (objetivo.equals("Objetivos")){
                loadChildCenter(objetivos);
            }

            laptopCollectionCenter.put(objetivo, childListCenter);
        }
        // preparing laptops collection(child)

        groupList = new ArrayList<String>();
        groupList.add("HP");
        groupList.add("Dell");
        groupList.add("Lenovo");
        groupList.add("Sony");
        groupList.add("HCL");
        groupList.add("Samsung");

        String[] hpModels = { "HP Pavilion G6-2014TX", "ProBook HP 4540",
                "HP Envy 4-1025TX" };
        String[] hclModels = { "HCL S2101", "HCL L2102", "HCL V2002" };
        String[] lenovoModels = { "IdeaPad Z Series", "Essential G Series",
                "ThinkPad X Series", "Ideapad Z Series" };
        String[] sonyModels = { "VAIO E Series", "VAIO Z Series",
                "VAIO S Series", "VAIO YB Series" };
        String[] dellModels = { "Inspiron", "Vostro", "XPS" };
        String[] samsungModels = { "NP Series", "Series 5", "SF Series" };

        matchCollection = new LinkedHashMap<String, List<String>>();

        for (String laptop : groupList) {
            if (laptop.equals("HP")) {
                loadChild(hpModels);
            } else if (laptop.equals("Dell"))
                loadChild(dellModels);
            else if (laptop.equals("Sony"))
                loadChild(sonyModels);
            else if (laptop.equals("HCL"))
                loadChild(hclModels);
            else if (laptop.equals("Samsung"))
                loadChild(samsungModels);
            else
                loadChild(lenovoModels);

            matchCollection.put(laptop, childList);
        }*/

        String idioma = Locale.getDefault().getDisplayLanguage();
        groupList = new ArrayList<String>();
        if (idioma.equals("English")){
            groupList.add("Red home objectives");
            groupList.add("Blue home objectives");
            groupList.add("Green home objectives");
            groupList.add("Center objectives");
        } else if (idioma.equals("español")){
            groupList.add("Objetivos hogar Rojo");
            groupList.add("Objetivos hogar Azul");
            groupList.add("Objetivos hogar Verde");
            groupList.add("Objetivos Central");
        }
        List<String[]> valoresTotales = new ArrayList<String[]>();
        String[] redHome = new String[17];
        String[] blueHome = new String[17];
        String[] greenHome = new String[17];
        String[] centerHome = new String[21];
        int contadorRed = 0;
        int contadorBlue = 0;
        int contadorGreen = 0;
        int contadorCenter = 0;
        String id = "";
        String nombreZona = "";
        String owner = "";
        String cadenaAux = "";
        String colorEquipo = "";
        for (int i = 0; i < listObjetivos.size(); i++){
            PartidoObjetivos partidoAux = listObjetivos.get(i);
            if (partidoAux.getType().equals("RedHome")){
                if (idioma.equals("English")){
                    id = partidoAux.getId();
                    nombreZona = getNombreZona(id);
                    owner = partidoAux.getOwner();
                    colorEquipo = getNombreEquipo(owner);
                    if (colorEquipo.equals("Neutral")){
                        cadenaAux = "Area with ID: "+id+" is neutral";
                    } else {
                        cadenaAux = nombreZona + " belongs to "+owner+" team";
                    }
                    redHome[contadorRed] = cadenaAux;
                    if (contadorRed < 16)
                        contadorRed++;
                } else if (idioma.equals("español")){
                    id = partidoAux.getId();
                    nombreZona = getNombreZona(id);
                    owner = partidoAux.getOwner();
                    colorEquipo = getNombreEquipo(owner);
                    if (colorEquipo.equals("Neutral")){
                        cadenaAux = "Zona con ID: "+id+" es neutral";
                    } else {
                        cadenaAux = nombreZona + " pertenece al equipo "+colorEquipo;
                    }
                    redHome[contadorRed] = cadenaAux;
                    if (contadorRed < 16)
                        contadorRed++;
                }

            } else if (partidoAux.getType().equals("BlueHome")){
                if (idioma.equals("English")){
                    id = partidoAux.getId();
                    nombreZona = getNombreZona(id);
                    owner = partidoAux.getOwner();
                    colorEquipo = getNombreEquipo(owner);
                    if (colorEquipo.equals("Neutral")){
                        cadenaAux = "Area with ID: "+id+" is neutral";
                    } else {
                        cadenaAux = nombreZona + " belongs to "+owner+" team";
                    }
                    blueHome[contadorBlue] = cadenaAux;
                    if (contadorBlue < 16)
                        contadorBlue++;
                } else if (idioma.equals("español")){
                    id = partidoAux.getId();
                    nombreZona = getNombreZona(id);
                    owner = partidoAux.getOwner();
                    colorEquipo = getNombreEquipo(owner);
                    if (colorEquipo.equals("Neutral")){
                        cadenaAux = "Zona con ID: "+id+" es neutral";
                    } else {
                        cadenaAux = nombreZona + " pertenece al equipo "+colorEquipo;
                    }
                    blueHome[contadorBlue] = cadenaAux;
                    if (contadorBlue < 16)
                        contadorBlue++;
                }
            } else if (partidoAux.getType().equals("GreenHome")){
                if (idioma.equals("English")){
                    id = partidoAux.getId();
                    nombreZona = getNombreZona(id);
                    owner = partidoAux.getOwner();
                    colorEquipo = getNombreEquipo(owner);
                    if (colorEquipo.equals("Neutral")){
                        cadenaAux = "Area with ID: "+id+" is neutral";
                    } else {
                        cadenaAux = nombreZona + " belongs to "+owner+" team";
                    }
                    greenHome[contadorGreen] = cadenaAux;
                    if (contadorGreen < 16)
                        contadorGreen++;
                } else if (idioma.equals("español")){
                    id = partidoAux.getId();
                    nombreZona = getNombreZona(id);
                    owner = partidoAux.getOwner();
                    colorEquipo = getNombreEquipo(owner);
                    if (colorEquipo.equals("Neutral")){
                        cadenaAux = "Zona con ID: "+id+" es neutral";
                    } else {
                        cadenaAux = nombreZona + " pertenece al equipo "+colorEquipo;
                    }
                    greenHome[contadorGreen] = cadenaAux;
                    if (contadorGreen < 16)
                        contadorGreen++;
                }
            } else if (partidoAux.getType().equals("Center")){
                if (idioma.equals("English")){
                    id = partidoAux.getId();
                    nombreZona = getNombreZona(id);
                    owner = partidoAux.getOwner();
                    colorEquipo = getNombreEquipo(owner);
                    if (colorEquipo.equals("Neutral")){
                        cadenaAux = "Area with ID: "+id+" is neutral";
                    } else {
                        cadenaAux = nombreZona + " belongs to "+owner+" team";
                    }
                    centerHome[contadorCenter] = cadenaAux;
                    if (contadorCenter < 20)
                        contadorCenter++;
                } else if (idioma.equals("español")){
                    id = partidoAux.getId();
                    nombreZona = getNombreZona(id);
                    owner = partidoAux.getOwner();
                    colorEquipo = getNombreEquipo(owner);
                    if (colorEquipo.equals("Neutral")){
                        cadenaAux = "Zona con ID: "+id+" es neutral";
                    } else {
                        cadenaAux = nombreZona + " pertenece al equipo "+colorEquipo;
                    }
                    centerHome[contadorCenter] = cadenaAux;
                    if (contadorCenter < 20)
                        contadorCenter++;
                }
            }
        }

        valoresTotales.add(redHome);
        valoresTotales.add(blueHome);
        valoresTotales.add(greenHome);
        valoresTotales.add(centerHome);
        matchCollection = new LinkedHashMap<String, List<String>>();
        for (String objetivo : groupList){

            if (idioma.equals("English")){
                if (objetivo.equals("Red home objectives")){
                    loadChild(valoresTotales.get(0));
                } else if (objetivo.equals("Blue home objectives")){
                    loadChild(valoresTotales.get(1));
                } else if (objetivo.equals("Green home objectives")){
                    loadChild(valoresTotales.get(2));
                } else if (objetivo.equals("Center home objectives")){
                    loadChild(valoresTotales.get(3));
                }
            } else if (idioma.equals("español")){
                if (objetivo.equals("Objetivos hogar Rojo")){
                    loadChild(valoresTotales.get(0));
                } else if (objetivo.equals("Objetivos hogar Azul")){
                    loadChild(valoresTotales.get(1));
                } else if (objetivo.equals("Objetivos hogar Verde")){
                    loadChild(valoresTotales.get(2));
                } else if (objetivo.equals("Objetivos hogar Central")){
                    loadChild(valoresTotales.get(3));
                }
            }
            matchCollection.put(objetivo, childList);
        }
    }

    private void loadChild(String[] laptopModels) {
        childList = new ArrayList<String>();
        for (String model : laptopModels)
            childList.add(model);
    }

    public void rellenarDatos(Partido partido, PartidoDetalles partidoD, PartidoMapas[] partidoMapas){
        TextView id_partido = (TextView)findViewById(R.id.textViewID_Partido);
        TextView id_mundoRojo = (TextView)findViewById(R.id.textViewIDMundo_Rojo);
        TextView id_mundoAzul = (TextView)findViewById(R.id.textViewIDMundo_Azul);
        TextView id_mundoVerde = (TextView)findViewById(R.id.textViewIDMundo_Verde);
        TextView fechaComienzo = (TextView)findViewById(R.id.textViewFecha_Comienzo);
        TextView fechaFinalizacion = (TextView)findViewById(R.id.textViewFecha_Finalizacion);
        TextView tiempoRestante = (TextView)findViewById(R.id.textViewTiempo_Restante);

        String fechaFinal = partido.getEnd_time();
        id_partido.setText(getResources().getString(R.string.id_partido)+" "+partido.getWvw_match_id());
        id_mundoRojo.setText(getResources().getString(R.string.red_world_id)+" "+partido.getBlue_world_id());
        id_mundoAzul.setText(getResources().getString(R.string.blue_world_id)+" "+partido.getRed_world_id());
        id_mundoVerde.setText(getResources().getString(R.string.green_world_id)+" "+partido.getGreen_world_id());
        fechaComienzo.setText(getResources().getString(R.string.fecha_comienzo)+" "+partido.getStart_time());
        fechaFinalizacion.setText(getResources().getString(R.string.fecha_finalizacion)+" "+fechaFinal);


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        Date fechaActualD = new Date();
        String fechaActualS = "";
        Date fechaFinalD = new Date();
        try {
            fechaFinalD = sdf.parse(fechaFinal.toString());
            fechaActualS = sdf.format(fechaActualD);
            fechaActualD = sdf.parse(fechaActualS.toString());
        } catch (ParseException e){
            e.getStackTrace();
        }

        String idioma = getResources().getString(R.string.id_partido);

        long[] diferenciaTemp = diferenciaEnDias(fechaFinalD, fechaActualD);
        if (diferenciaTemp[0] == 0){
            if (idioma.equals("Match ID:")){
                tiempoRestante.setText(getResources().getString(R.string.tiempo_restante) + " "
                        + diferenciaTemp[1] + " days");
            }
            else {
                tiempoRestante.setText(getResources().getString(R.string.tiempo_restante) + " "
                        + diferenciaTemp[1] + " días");
            }
        } else if (diferenciaTemp[0] == 1) {
            if (idioma.equals("Match ID:")){
                tiempoRestante.setText(getResources().getString(R.string.tiempo_restante)+" "
                        +diferenciaTemp[1]+" hours");
            } else{
                tiempoRestante.setText(getResources().getString(R.string.tiempo_restante)+" "
                        +diferenciaTemp[1]+" horas");
            }
        } else if (diferenciaTemp[0] == 2) {
            if (idioma.equals("Match ID:")){
                tiempoRestante.setText(getResources().getString(R.string.tiempo_restante)+" "
                        +diferenciaTemp[1]+" minutes");
            } else {
                tiempoRestante.setText(getResources().getString(R.string.tiempo_restante)+" "
                        +diferenciaTemp[1]+" minutos");
            }
        } else {
            if (idioma.equals("Match ID:")){
                tiempoRestante.setText(getResources().getString(R.string.tiempo_restante)+" "
                        +diferenciaTemp[1]+" seconds");
            } else {
                tiempoRestante.setText(getResources().getString(R.string.tiempo_restante)+" "
                        +diferenciaTemp[1]+" segundos");
            }
        }

        TextView textViewPuntuacionRojo = (TextView)findViewById(R.id.textViewPuntuacionRojo);
        TextView textViewPuntuacionAzul = (TextView)findViewById(R.id.textViewPuntuacionAzul);
        TextView textViewPuntuacionVerde = (TextView)findViewById(R.id.textViewPuntuacionVerde);

        long[] puntuaciones = partidoD.getScores();
        textViewPuntuacionRojo.setText(getResources().getString(R.string.puntuacion_rojo)+" "+puntuaciones[0]);
        textViewPuntuacionAzul.setText(getResources().getString(R.string.puntuacion_azul)+" "+puntuaciones[1]);
        textViewPuntuacionVerde.setText(getResources().getString(R.string.puntuacion_verde)+" "+puntuaciones[2]);

        TextView textViewRedHomeRojo = (TextView)findViewById(R.id.textViewRedHomeRojo);
        TextView textViewRedHomeAzul = (TextView)findViewById(R.id.textViewRedHomeAzul);
        TextView textViewRedHomeVerde = (TextView)findViewById(R.id.textViewRedHomeVerde);

        long[] scoresRojo = partidoMapas[0].getScores();

        textViewRedHomeRojo.setText(getResources().getString(R.string.puntuacion_rojo)+" "+scoresRojo[0]);
        textViewRedHomeAzul.setText(getResources().getString(R.string.puntuacion_azul)+" "+scoresRojo[1]);
        textViewRedHomeVerde.setText(getResources().getString(R.string.puntuacion_verde)+" "+scoresRojo[2]);

        TextView textViewBlueHomeRojo = (TextView)findViewById(R.id.textViewBlueHomeRojo);
        TextView textViewBlueHomeAzul = (TextView)findViewById(R.id.textViewBlueHomeAzul);
        TextView textViewBlueHomeVerde = (TextView)findViewById(R.id.textViewBlueHomeVerde);

        long[] scoresAzul = partidoMapas[1].getScores();

        textViewBlueHomeRojo.setText(getResources().getString(R.string.puntuacion_rojo)+" "+scoresAzul[0]);
        textViewBlueHomeAzul.setText(getResources().getString(R.string.puntuacion_azul)+" "+scoresAzul[1]);
        textViewBlueHomeVerde.setText(getResources().getString(R.string.puntuacion_verde)+" "+scoresAzul[2]);

        TextView textViewGreenHomeRojo = (TextView)findViewById(R.id.textViewGreenHomeRojo);
        TextView textViewGreenHomeAzul = (TextView)findViewById(R.id.textViewGreenHomeAzul);
        TextView textViewGreenHomeVerde = (TextView)findViewById(R.id.textViewGreenHomeVerde);

        long[] scoresVerde = partidoMapas[2].getScores();

        textViewGreenHomeRojo.setText(getResources().getString(R.string.puntuacion_rojo)+" "+scoresVerde[0]);
        textViewGreenHomeAzul.setText(getResources().getString(R.string.puntuacion_azul)+" "+scoresVerde[1]);
        textViewGreenHomeVerde.setText(getResources().getString(R.string.puntuacion_verde)+" "+scoresVerde[2]);

        TextView textViewCenterHomeRojo = (TextView)findViewById(R.id.textViewCenterHomeRojo);
        TextView textViewCenterHomeAzul = (TextView)findViewById(R.id.textViewCenterHomeAzul);
        TextView textViewCenterHomeVerde = (TextView)findViewById(R.id.textViewCenterHomeVerde);

        long[] scoresCenter = partidoMapas[3].getScores();

        textViewCenterHomeRojo.setText(getResources().getString(R.string.puntuacion_rojo)+" "+scoresCenter[0]);
        textViewCenterHomeAzul.setText(getResources().getString(R.string.puntuacion_azul)+" "+scoresCenter[1]);
        textViewCenterHomeVerde.setText(getResources().getString(R.string.puntuacion_verde)+" "+scoresCenter[2]);
    }

    public long[] diferenciaEnDias (Date fechaMayor, Date fechaMenor){
        // Las 4 líneas posteriores se usaron para comprobar si se hacía la visualización
        // de la diferencia de horas, minutos y segundos.
        //Calendar cal = Calendar.getInstance(); // creates calendar
        //cal.setTime(new Date()); // sets calendar time/date
        //cal.add(Calendar.SECOND, 37); // adds one hour
        //fechaMayor = cal.getTime(); // returns new date object, one hour in the future
        long diferenciaEn_ms = fechaMayor.getTime() - fechaMenor.getTime();
        long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24);
        long[] valores = new long[2];
        long horas = 0;
        long minutos = 0;
        long segundos = 0;
        if (dias >= 1){
            valores[0] = 0;
            valores[1] = dias;
        } else if (dias < 1){
            horas = diferenciaEn_ms / (1000 * 60 * 60);
            minutos = diferenciaEn_ms / (1000 * 60);
            segundos = diferenciaEn_ms / (1000);
            if (horas <= 0){
                if (minutos <= 0){
                    valores[0] = 3;
                    valores[1] = segundos;
                } else {
                    valores[0] = 2;
                    valores[1] = minutos;
                }

            } else {
                valores[0] = 1;
                valores[1] = horas;
            }
        }
        return valores;
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if (requestCode == )
        //String valor = data.getStringExtra()

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_partido_detalles_actividad, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.refrescar_overflow || id == R.id.refrescar) {
            JSONParserPartidosD json = new JSONParserPartidosD(this, partidoDetalles_actividad.this, true);
            try {
                json.recuperarDetalles(partidoID);
            } catch (Exception e){
                e.getStackTrace();
            }
            return true;
        } else if (id == R.id.temporizador || id == R.id.temporizador_overflow) {

            if (countDownTimer != null){
                countDownTimer.cancel();
            }

            LayoutInflater li = LayoutInflater.from(this);
            View dialogo = li.inflate(R.layout.dialogo_temporizador, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setView(dialogo);
            final EditText et = (EditText)dialogo.findViewById(R.id.tiempo_refresco);

            String boton_cancelar = getResources().getString(R.string.boton_cancelar);
            // Mostramos el mensaje del cuadro de dialogo
            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // Rescatamos el nombre del EditText

                            segundos = 0;
                            String cadenaIntroducida = et.getText().toString();

                            if (cadenaIntroducida.length() < 1){
                                dialog.cancel();
                            } else {
                                segundos = Integer.parseInt(cadenaIntroducida);
                                if (segundos <= 10){

                                    String recomendacion = getResources().getString(R.string.recomendacion);
                                    Toast.makeText(getBaseContext(), recomendacion, Toast.LENGTH_LONG)
                                            .show();
                                } else {
                                    // 1 minuto = 60.000 milisegundos
                                    // 1 segundo = 1.000 milisegundos
                                    segundos = segundos * 1000;
                                    countDownTimer = new PartidosCountDownTimer(startTime, segundos);
                                    countDownTimer.start();
                                }
                            }
                        }
                    })
                    .setNegativeButton(boton_cancelar, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // Cancelamos el cuadro de dialogo
                            dialog.cancel();
                            //countDownTimer.start();
                        }
                    });

            // Creamos un AlertDialog y lo mostramos
            AlertDialog alertDialog = alertDialogBuilder.create();

            alertDialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void estructurarLista(){
        if (partidoID.equals("2-9")){
            partidoID = "2-9";
        } else if (partidoID.equals("2-2")){
            partidoID = "2-2";
        } else if (partidoID.equals("2-7")){
            partidoID = "2-7";
        } else if (partidoID.equals("2-1")){
            partidoID = "2-1";
        } else if (partidoID.equals("2-4")){
            partidoID = "2-4";
        } else if (partidoID.equals("2-6")){
            partidoID = "2-6";
        } else if (partidoID.equals("2-8")){
            partidoID = "2-8";
        } else if (partidoID.equals("2-5")){
            partidoID = "2-5";
        } else if (partidoID.equals("2-3")){
            partidoID = "2-3";
        } else if (partidoID.equals("1-1")){
            partidoID = "1-1";
        } else if (partidoID.equals("1-2")){
            partidoID = "1-2";
        } else if (partidoID.equals("1-3")){
            partidoID = "1-3";
        } else if (partidoID.equals("1-4")){
            partidoID = "1-4";
        } else if (partidoID.equals("1-5")){
            partidoID = "1-5";
        } else if (partidoID.equals("1-6")){
            partidoID = "1-6";
        } else if (partidoID.equals("1-7")){
            partidoID = "1-7";
        } else if (partidoID.equals("1-8")){
            partidoID = "1-8";
        }
    }

    public void expandableRefrescar(){
        expListView = (ExpandableListView) findViewById(R.id.expandableListView);
        final ExpandableListAdapterObjetivos expListAdapter = new ExpandableListAdapterObjetivos(
                this, groupList, matchCollection);
        expListView.setAdapter(expListAdapter);
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                final String selected = (String) expListAdapter.getChild(
                        groupPosition, childPosition);
                Toast.makeText(getBaseContext(), selected, Toast.LENGTH_LONG)
                        .show();

                return true;
            }
        });
    }
}
