package es.uniovi.benja.partidosgw2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cyrak on 24/3/15.
 */
public class PartidoMapas {

    // Área de datos
    private String id;
    private String type;
    private long[] scores;
    private List<PartidoObjetivos> listaObjetivos;

    public PartidoMapas() {
        listaObjetivos = new ArrayList<PartidoObjetivos>();
    }

    public PartidoMapas (String type, long[] scores){
        this.type = type;
        this.scores[0] = scores[0];
        this.scores[1] = scores[1];
        this.scores[2] = scores[2];
        listaObjetivos = new ArrayList<PartidoObjetivos>();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long[] getScores() {
        return scores;
    }

    public void setScores(long[] scores) {
        this.scores = scores;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
