package es.uniovi.benja.partidosgw2;

/**
 * Created by cyrak on 24/3/15.
 */
public class PartidoObjetivos {

    // Área de datos
    private String id_global;
    private String id;
    private String type;
    private String owner;

    public PartidoObjetivos (){}

    public PartidoObjetivos (String id, String owner){
        this.id = id;
        this.owner = owner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getId_global() {
        return id_global;
    }

    public void setId_global(String id_global) {
        this.id_global = id_global;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
