package es.uniovi.benja.partidosgw2;

/**
 * Created by cyrak on 23/3/15.
 */
public class PartidoDetalles {

    // Área de datos
    private String match_id;
    private long[] scores;

    public PartidoDetalles(){

    }

    public PartidoDetalles (String match_idAux, long[] scoresAux){
        match_id = match_idAux;
        scores = new long[3];
        scores[0] = scoresAux[0];
        scores[1] = scoresAux[1];
        scores[2] = scoresAux[2];
    }

    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public long[] getScores() {
        return scores;
    }

    public void setScores(long[] scores) {
        this.scores = scores;
    }
}
