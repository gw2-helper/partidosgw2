package es.uniovi.benja.partidosgw2;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Handler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by cyrak on 14/3/15.
 */
public class JSONParserPartidosD extends Thread {

    private Activity activity;
    private JSONObject jObject;
    private JSONObject jObject2;
    private JSONArray partidosArray;
    private ProgressDialog progressDialog = null;
    private Runnable runReadAndParseJSON;
    private List<String> partidosIDs;
    private JSONArray puntuacionIndividual;
    private JSONArray mapasIndividuales;
    private Handler mHandler = new Handler();
    private partidoDetalles_actividad referenciaD;
    private long blueworld;
    private long greenworld;
    private long redworld;
    private String start_time;
    private String end_time;
    private Date fechaComienzo;
    private Date fechaFinal;
    private Date fechaActual;
    private static final String LOGTAG = "LogsAndroid";
    private boolean comprobacion;
    public List<Partido> listaPartidos;
    private List<PartidoDetalles> listaPartidoDetalles;
    private List<PartidoMapas> listaPartidoMapas;
    private List<PartidoObjetivos> listaPartidoObjetivos;
    private Partido partidoDetalle;
    private PartidoDetalles partidoDetalles;
    private PartidoMapas[] partidoMapas;
    private boolean refrescar;

    //Map<Integer, Map> mapas = new LinkedHashMap<String, Integer>();


    public JSONParserPartidosD(Activity a, partidoDetalles_actividad referenciaObtenida, boolean refrescar){
        activity = a;
        puntuacionIndividual = new JSONArray();
        referenciaD = referenciaObtenida;
        partidosIDs = new ArrayList<String>();
        comprobacion = false;
        mapasIndividuales = new JSONArray();
        listaPartidoDetalles = new ArrayList<PartidoDetalles>();
        listaPartidoMapas = new ArrayList<PartidoMapas>();
        listaPartidoObjetivos = new ArrayList<PartidoObjetivos>();
        listaPartidos = new ArrayList<Partido>();
        partidoDetalle = new Partido();
        partidoDetalles = new PartidoDetalles();
        partidoMapas = new PartidoMapas[4];
        this.refrescar = refrescar;
    }



    public List<Partido> getPartidosObtenidos (){
        return listaPartidos;
    }

    public void obtenerIDs (){
        for (int i = 0; i < listaPartidos.size(); i++){
            partidosIDs.add(((Partido)listaPartidos.get(i)).getWvw_match_id());
        }
    }


    public void recuperarDetalles(String idPartido) throws JSONException {
        final String id_partido = idPartido;
        runReadAndParseJSON = new Runnable() {
            @Override
            public void run() {
                try{
                    recuperarDetallesCompleto(id_partido);

                } catch(Exception e){}
            }
        };
        Thread thread = new Thread(null, runReadAndParseJSON,"bgReadJSONPartidos");
        thread.start();
        String idioma = Locale.getDefault().getDisplayLanguage();

        if (idioma.equals("English")){
            progressDialog = ProgressDialog.show(activity, "Downloading information", "Please, wait",true);
        } else if (idioma.equals("español")){
            progressDialog = ProgressDialog.show(activity, "Descargando información", "Por favor espere",true);
        }

    }

    public void readAndParseJSONPartidos() throws JSONException {

        runReadAndParseJSON = new Runnable() {
            @Override
            public void run() {
                try{
                    readJSONPartidos();
                } catch(Exception e){}
            }
        };
        Thread thread = new Thread(null, runReadAndParseJSON,"bgReadJSONPartidos");
        thread.start();
        String idioma = Locale.getDefault().getDisplayLanguage();

        if (idioma.equals("English")){
            progressDialog = ProgressDialog.show(activity, "Downloading information", "Please, wait",true);
        } else if (idioma.equals("español")){
            progressDialog = ProgressDialog.show(activity, "Descargando información", "Por favor espere",true);
        }
    }

    private Runnable returnRes = new Runnable(){
        @Override
        public void run() {
            progressDialog.dismiss();
            /*
            blueworld = ((Partido)listaPartidos.get(0)).getBlue_world_id();
            redworld = ((Partido)listaPartidos.get(0)).getRed_world_id();
            greenworld = ((Partido)listaPartidos.get(0)).getGreen_world_id();
            start_time = ((Partido)listaPartidos.get(0)).getStart_time();
            end_time = ((Partido)listaPartidos.get(0)).getEnd_time();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
            fechaActual = new Date();
            try {
                fechaComienzo = sdf.parse(start_time);
                fechaFinal = sdf.parse(end_time);
                fechaActual = sdf.parse(fechaActual.toString());
            } catch (ParseException e){
                e.getStackTrace();
            }*/

            //referencia.rellenarMundos(listaPartidos);
            //referencia.rellenarLista(listaPartidos)

            //referenciaD.createGroupList();
            if (refrescar){
                referenciaD.createCollection(listaPartidoObjetivos);
                referenciaD.expandableRefrescar();
                referenciaD.rellenarDatos(partidoDetalle, partidoDetalles, partidoMapas);
            } else {
                referenciaD.createCollection(listaPartidoObjetivos);
                referenciaD.expandable();
                referenciaD.rellenarDatos(partidoDetalle, partidoDetalles, partidoMapas);
            }
        }
    };

    private void recuperarDetallesCompleto(String idPartido) throws JSONException {

        jObject = JSONManager.getJSONfromURL("https://api.guildwars2.com/v1/wvw/matches.json");
        if(jObject != null)
            parseJSONPartidos(jObject.getJSONArray("wvw_matches"));

        List<Partido> partidos = new ArrayList<Partido>();
        partidos = getPartidosObtenidos();


        for (int i = 0; i < partidos.size(); i++){
            if (partidos.get(i).getWvw_match_id().equals(idPartido)){
                partidoDetalle.setWvw_match_id(partidos.get(i).getWvw_match_id());
                partidoDetalle.setRed_world_id(partidos.get(i).getRed_world_id());
                partidoDetalle.setBlue_world_id(partidos.get(i).getBlue_world_id());
                partidoDetalle.setGreen_world_id(partidos.get(i).getGreen_world_id());
                partidoDetalle.setStart_time(partidos.get(i).getStart_time());
                partidoDetalle.setEnd_time(partidos.get(i).getEnd_time());
            }
        }




        jObject2 = JSONManager.getJSONfromURL(
                "https://api.guildwars2.com/v1/wvw/match_details.json?match_id="+idPartido);
        if (jObject2 != null){
            puntuacionIndividual = jObject2.getJSONArray("scores");
            mapasIndividuales = jObject2.getJSONArray("maps");
        }

        long[] puntuacionesD = new long[3];
        puntuacionesD[0] = Long.parseLong(puntuacionIndividual.get(0).toString());
        puntuacionesD[1] = Long.parseLong(puntuacionIndividual.get(1).toString());
        puntuacionesD[2] = Long.parseLong(puntuacionIndividual.get(2).toString());
        partidoDetalles.setMatch_id(idPartido);
        partidoDetalles.setScores(puntuacionesD);
        //listaPartidoDetalles.add(partidoD);


        long[] puntuacionesM = new long[3];

        JSONArray objetivos = new JSONArray();
        JSONObject objetivoIndividual = new JSONObject();
        String type = "";
        String scoreIn = "";

        // ¿7?
        for (int j = 0; j < 4; j++){
            PartidoMapas partidoM = new PartidoMapas();
            long[] puntuacionesM_Aux = new long[3];
            partidoM.setId(idPartido);
            type = (mapasIndividuales.getJSONObject(j)).getString("type");
            partidoM.setType(type);
            scoreIn =
                    ((mapasIndividuales.getJSONObject(j).getJSONArray("scores")).get(0)).toString();
            puntuacionesM[0] = Long.parseLong(scoreIn);
            scoreIn = ((mapasIndividuales.getJSONObject(j).getJSONArray("scores")).get(1)).toString();
            puntuacionesM[1] = Long.parseLong(scoreIn);
            scoreIn = ((mapasIndividuales.getJSONObject(j).getJSONArray("scores")).get(2)).toString();
            puntuacionesM[2] = Long.parseLong(scoreIn);
            objetivos = (mapasIndividuales.getJSONObject(j).getJSONArray("objectives"));
            puntuacionesM_Aux = puntuacionesM.clone();
            partidoM.setScores(puntuacionesM_Aux);
            PartidoMapas partidoM_Aux = new PartidoMapas();
            partidoM_Aux = partidoM;
            listaPartidoMapas.add(partidoM_Aux);

            for (int k = 0; k < objetivos.length(); k++){
                PartidoObjetivos partidoO = new PartidoObjetivos();
                partidoO.setId_global(idPartido);
                partidoO.setType(type);
                objetivoIndividual = objetivos.getJSONObject(k);
                partidoO.setId(objetivoIndividual.getString("id"));
                partidoO.setOwner(objetivoIndividual.getString("owner"));
                listaPartidoObjetivos.add(partidoO);
            }
        }
        PartidoMapas redHome = new PartidoMapas();
        PartidoMapas blueHome = new PartidoMapas();
        PartidoMapas greenHome = new PartidoMapas();
        PartidoMapas centerHome = new PartidoMapas();

        for (int i = 0; i < listaPartidoMapas.size(); i++){
            if (listaPartidoMapas.get(i).getType().equals("RedHome")){
                redHome.setId(listaPartidoMapas.get(i).getId());
                redHome.setScores(listaPartidoMapas.get(i).getScores());
                redHome.setType(listaPartidoMapas.get(i).getType());
            } else if (listaPartidoMapas.get(i).getType().equals("GreenHome")){
                greenHome.setId(listaPartidoMapas.get(i).getId());
                greenHome.setScores(listaPartidoMapas.get(i).getScores());
                greenHome.setType(listaPartidoMapas.get(i).getType());
            } else if (listaPartidoMapas.get(i).getType().equals("BlueHome")){
                blueHome.setId(listaPartidoMapas.get(i).getId());
                blueHome.setScores(listaPartidoMapas.get(i).getScores());
                blueHome.setType(listaPartidoMapas.get(i).getType());
            } else if (listaPartidoMapas.get(i).getType().equals("Center")){
                centerHome.setId(listaPartidoMapas.get(i).getId());
                centerHome.setScores(listaPartidoMapas.get(i).getScores());
                centerHome.setType(listaPartidoMapas.get(i).getType());
            }
        }

        partidoMapas[0] = redHome;
        partidoMapas[1] = blueHome;
        partidoMapas[2] = greenHome;
        partidoMapas[3] = centerHome;
        activity.runOnUiThread(returnRes);
    }

    private void readJSONPartidos() throws JSONException{
        jObject = JSONManager.getJSONfromURL("https://api.guildwars2.com/v1/wvw/matches.json");
        if(jObject != null)
            parseJSONPartidos(jObject.getJSONArray("wvw_matches"));

        // ¡OJO! HACER ARRAYLIST DE PARTIDO Y ALMACENAR LOS DATOS.
        /*
        obtenerIDs();
        for (int i = 0; i < partidosIDs.size(); i++){
            String id = partidosIDs.get(i).toString();
            jObject2 = JSONManager.getJSONfromURL(
                    "https://api.guildwars2.com/v1/wvw/match_details.json?match_id="+id);
            if (jObject2 != null){
                puntuacionIndividual = jObject2.getJSONArray("scores");
                mapasIndividuales = jObject2.getJSONArray("maps");
            }

            PartidoDetalles partidoD = new PartidoDetalles();
            long[] puntuacionesD = new long[3];
            puntuacionesD[0] = Long.parseLong(puntuacionIndividual.get(0).toString());
            puntuacionesD[1] = Long.parseLong(puntuacionIndividual.get(1).toString());
            puntuacionesD[2] = Long.parseLong(puntuacionIndividual.get(2).toString());
            partidoD.setMatch_id(id);
            partidoD.setScores(puntuacionesD);
            listaPartidoDetalles.add(partidoD);

            long[] puntuacionesM = new long[3];
            JSONArray objetivos = new JSONArray();
            JSONObject objetivoIndividual = new JSONObject();
            String type = "";
            String scoreIn = "";

            for (int j = 0; j < mapasIndividuales.length(); j++){
                PartidoMapas partidoM = new PartidoMapas();
                partidoM.setId(id);
                type = (mapasIndividuales.getJSONObject(i)).getString("type");
                partidoM.setType(type);
                scoreIn =
                        ((mapasIndividuales.getJSONObject(i).getJSONArray("scores")).get(0)).toString();
                puntuacionesM[0] = Long.parseLong(scoreIn);
                scoreIn = ((mapasIndividuales.getJSONObject(i).getJSONArray("scores")).get(1)).toString();
                puntuacionesM[1] = Long.parseLong(scoreIn);
                scoreIn = ((mapasIndividuales.getJSONObject(i).getJSONArray("scores")).get(2)).toString();
                puntuacionesM[2] = Long.parseLong(scoreIn);
                objetivos = (mapasIndividuales.getJSONObject(i).getJSONArray("objectives"));
                partidoM.setScores(puntuacionesM);
                listaPartidoMapas.add(partidoM);
                for (int k = 0; k < objetivos.length(); k++){
                    PartidoObjetivos partidoO = new PartidoObjetivos();
                    partidoO.setId_global(id);
                    objetivoIndividual = objetivos.getJSONObject(i);
                    partidoO.setId(objetivoIndividual.getString("id"));
                    partidoO.setOwner(objetivoIndividual.getString("owner"));
                    listaPartidoObjetivos.add(partidoO);
                }
            }
        }*/

        activity.runOnUiThread(returnRes);
    }

    private void parseJSONPartidos(JSONArray partidosArray) throws JSONException{
        for(int i = 0; i < partidosArray.length(); i++){
            Partido l = new Partido();
            /*
            protected String wvw_match_id;
            protected long red_world_id;
            protected long blue_world_id;
            protected long green_world_id;
            protected Date start_time;
            protected Date end_time;
             */
            l.setWvw_match_id(partidosArray.getJSONObject(i).getString("wvw_match_id"));
            l.setRed_world_id(partidosArray.getJSONObject(i).getLong("red_world_id"));
            l.setBlue_world_id(partidosArray.getJSONObject(i).getLong("blue_world_id"));
            l.setGreen_world_id(partidosArray.getJSONObject(i).getLong("green_world_id"));
            l.setStart_time(partidosArray.getJSONObject(i).getString("start_time"));
            l.setStart_time(partidosArray.getJSONObject(i).getString("start_time"));
            l.setEnd_time(partidosArray.getJSONObject(i).getString("end_time"));

            listaPartidos.add(l);
        }
    }
}
