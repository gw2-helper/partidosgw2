package es.uniovi.benja.partidosgw2;

import java.util.List;
import java.util.Map;

//import com.theopentutorials.expandablelist.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Activity context;
    private Map<String, List<String>> laptopCollections;
    private List<String> laptops;
    public final static String EXTRA_MESSAGE = "es.uniovi.benja.partidosgw2.MESSAGE";
    public final static int RESULT_MENSAJE = 1;
    public static final long PARTIDO_ID = 0;
    private List<String> id_partidos;

    public ExpandableListAdapter(Activity context, List<String> laptops,
                                 Map<String, List<String>> laptopCollections,
                                 List<String> id_partidos) {
        this.context = context;
        this.laptopCollections = laptopCollections;
        this.laptops = laptops;
        this.id_partidos = id_partidos;
    }

    public Object getChild(int groupPosition, int childPosition) {
        return laptopCollections.get(laptops.get(groupPosition)).get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String laptop = (String) getChild(groupPosition, childPosition);
        LayoutInflater inflater = context.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.child_item, null);
        }

        TextView item = (TextView) convertView.findViewById(R.id.laptop);
        /*
        ImageView delete = (ImageView) convertView.findViewById(R.id.delete);
        delete.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Do you want to remove?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                List<String> child =
                                        laptopCollections.get(laptops.get(groupPosition));
                                child.remove(childPosition);
                                notifyDataSetChanged();
                            }
                        });
                builder.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });*/

        item.setText(laptop);
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        //return this.laptopCollections.get(this.laptops.get(groupPosition)).size();
        return laptopCollections.get(laptops.get(groupPosition)).size();
        // laptopCollections: Map<String, List<String>>
        // laptops: List<String>
    }

    public Object getGroup(int groupPosition) {
        return laptops.get(groupPosition);
    }

    public int getGroupCount() {
        return laptops.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        final int posicion = groupPosition;
        String laptopName = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.group_item,
                    null);

        }
        TextView item = (TextView) convertView.findViewById(R.id.laptop);
        item.setTypeface(null, Typeface.BOLD);
        item.setText(laptopName);

        Button addButton = (Button)convertView.findViewById(R.id.infobutton);
        //addButton.setBackgroundColor();


        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // partidoID es la posición de la lista que estoy clickando.
                // NUEVO
                long ID = getGroupId(posicion);
               // ANTES:  long partidoID = getGroupId(posicion);
                // NUEVO
                String partidoID = id_partidos.get(Integer.parseInt(ID+""));

                // En ids_partidos tengo la lista de las IDs en orden correcto.


                //Object obj = getGroup(posicion);

                //this.getResources().getString(R.string.id_partido

                Intent intent = new Intent(context, partidoDetalles_actividad.class);
                intent.putExtra(EXTRA_MESSAGE, ""+partidoID);
                context.startActivityForResult(intent, RESULT_MENSAJE);
            }
        });

        /*
        if (isExpanded) {
            convertView.setBackgroundResource(R.drawable.abc_cab_background_top_material);
        } else {
            convertView.setBackgroundResource(R.drawable.abc_cab_background_internal_bg);
        }*/
        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}