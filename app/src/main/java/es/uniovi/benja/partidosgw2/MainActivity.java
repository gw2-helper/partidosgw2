package es.uniovi.benja.partidosgw2;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends ActionBarActivity {

    // Área de datos
    private long blueworld = 0;
    private long redworld = 0;
    private long greenworld = 0;
    private List<Partido> listaP;
    //private RecyclerView recycler;
    //private RecyclerView.Adapter adapter;
    //private RecyclerView.LayoutManager lManager;

    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    private ListView list;
    private boolean hayConexion;
    List<String> groupList;
    // NUEVO
    public List<String> id_partidos;
    List<String> childList;
    Map<String, List<String>> laptopCollection;
    private ExpandableListView expListView;

    /*
    public void setLista(List<Partido> listaPartido){
        listaP = listaPartido;
    }*/

    // Detección de la conexión: Sesión nº10 de Prácticas de Laboratorio
    boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public List<String> getListaPartidos(){
        return id_partidos;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listaP = new ArrayList<Partido>();
        hayConexion = isOnline();

        if (hayConexion){
            JSONParserPartidos json = new JSONParserPartidos(this, MainActivity.this);
            try {
                json.readAndParseJSONPartidos();
            } catch (Exception e){
                e.getStackTrace();
            }
        } else {
            Toast.makeText(getBaseContext(),
                    "No se puede establecer conexión, por favor, compruebe el estado de la red.",
                    Toast.LENGTH_LONG)
                    .show();
        }


        //createGroupList();
        //createCollection();

    }

    public void expandable(){
        expListView = (ExpandableListView) findViewById(R.id.laptop_list);
        final ExpandableListAdapter expListAdapter = new ExpandableListAdapter(
                this, groupList, laptopCollection, id_partidos);
        expListView.setAdapter(expListAdapter);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                final String selected = (String) expListAdapter.getChild(
                        groupPosition, childPosition);
                Toast.makeText(getBaseContext(), selected, Toast.LENGTH_LONG)
                        .show();

                return true;
            }
        });
    }

    public void createGroupList(List<Partido> listaP) {

        String cadena = "";
        groupList = new ArrayList<String>();
        // NUEVO
        id_partidos = new ArrayList<String>();
        List<String[]> valoresTotales = new ArrayList<String[]>();
//
//        groupList.add("2-5");
//        groupList.add("2-6");
//        groupList.add("2-4");
//        groupList.add("2-8");
//        groupList.add("2-1");
//        groupList.add("2-3");

        String[] valores = new String[6];
        String[] valoresAux = new String[6];


        for (int i = 0; i < listaP.size(); i++){
            cadena = getResources().getString(R.string.id_partido);
            cadena = cadena + " " + listaP.get(i).getWvw_match_id();
            // NUEVO
            id_partidos.add(listaP.get(i).getWvw_match_id());
            groupList.add(cadena);

            // Valores de los mundos:
            cadena = getResources().getString(R.string.red_world_id);
            cadena = cadena + " " + listaP.get(i).getRed_world_id();

            valores[0] = cadena;
            cadena = getResources().getString(R.string.blue_world_id);
            cadena = cadena + " " + listaP.get(i).getBlue_world_id();

            valores[1] = cadena;
            cadena = getResources().getString(R.string.green_world_id);
            cadena = cadena + " " + listaP.get(i).getGreen_world_id();

            valores[2] = cadena;

            // Valores de las fechas:
            cadena = getResources().getString(R.string.fecha_comienzo);
            cadena = cadena + " " + listaP.get(i).getStart_time();
            valores[3] = cadena;
            cadena = getResources().getString(R.string.fecha_finalizacion);
            cadena = cadena + " " + listaP.get(i).getEnd_time();
            valores[4] = cadena;
            cadena = getResources().getString(R.string.tiempo_restante);

            String fechaFinal = listaP.get(i).getEnd_time();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
            Date fechaActualD = new Date();
            String fechaActualS = "";
            Date fechaFinalD = new Date();
            try {
                fechaFinalD = sdf.parse(fechaFinal.toString());
                fechaActualS = sdf.format(fechaActualD);
                fechaActualD = sdf.parse(fechaActualS.toString());
            } catch (ParseException e){
                e.getStackTrace();
            }

            String idioma = getResources().getString(R.string.id_partido);

            long[] diferenciaTemp = diferenciaEnDias(fechaFinalD, fechaActualD);
            if (diferenciaTemp[0] == 0){
                if (idioma.equals("Match ID:"))
                    cadena = cadena +" "+diferenciaTemp[1] +" days";
                else
                    cadena = cadena +" "+diferenciaTemp[1] +" días";
            } else if (diferenciaTemp[0] == 1) {
                if (idioma.equals("Match ID:"))
                    cadena = cadena +" "+diferenciaTemp[1] +" hours";
                else
                    cadena = cadena +" "+diferenciaTemp[1] +" horas";
            } else if (diferenciaTemp[0] == 2) {
                if (idioma.equals("Match ID:"))
                    cadena = cadena +" "+diferenciaTemp[1] +" minutes";
                else
                    cadena = cadena +" "+diferenciaTemp[1] +" minutos";
            } else {
                if (idioma.equals("Match ID:"))
                    cadena = cadena +" "+diferenciaTemp[1] +" seconds";
                else
                    cadena = cadena +" "+diferenciaTemp[1] +" segundos";
            }

            valores[5] = cadena;
            valoresAux = valores.clone();
            valoresTotales.add(valoresAux);
        }

        laptopCollection = new LinkedHashMap<String, List<String>>();
        for (String partido : groupList){
            if (partido.equals(getResources().getString(R.string.id_partido)+" 2-9")){
                loadChild(valoresTotales.get(0));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 2-2")){
                loadChild(valoresTotales.get(1));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 2-7")){
                loadChild(valoresTotales.get(2));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 2-1")){
                loadChild(valoresTotales.get(3));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 2-4")){
                loadChild(valoresTotales.get(4));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 2-6")){
                loadChild(valoresTotales.get(5));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 2-8")){
                loadChild(valoresTotales.get(6));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 2-5")){
                loadChild(valoresTotales.get(7));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 2-3")){
                loadChild(valoresTotales.get(8));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 1-1")){
                loadChild(valoresTotales.get(9));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 1-2")){
                loadChild(valoresTotales.get(10));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 1-3")){
                loadChild(valoresTotales.get(11));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 1-4")){
                loadChild(valoresTotales.get(12));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 1-5")){
                loadChild(valoresTotales.get(13));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 1-6")){
                loadChild(valoresTotales.get(14));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 1-7")){
                loadChild(valoresTotales.get(15));
            } else if (partido.equals(getResources().getString(R.string.id_partido)+" 1-8")){
            //if (partido.equals(getResources().getString(R.string.id_partido)+" 1-8")){
                loadChild(valoresTotales.get(16));
            }

            laptopCollection.put(partido, childList);
        }
    }


    private void loadChild(String[] laptopModels) {
        childList = new ArrayList<String>();
        for (String model : laptopModels)
            childList.add(model);
    }

    private void setGroupIndicatorToRight() {
        /* Get the screen width */
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;

        expListView.setIndicatorBounds(width - getDipsFromPixel(35), width
                - getDipsFromPixel(5));
    }

    // Convert pixel to dip
    public int getDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    /*
    public void rellenarLista(List<Partido> listaPartidos){
        list = (ListView)findViewById(R.id.listView);
        arrayList = new ArrayList<String>();
        String cadena = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaComienzo = new Date();
        Date fechaFinal = new Date();
        Date fechaActual = new Date();

        for (int i = 0; i < listaPartidos.size(); i++){
            cadena = getResources().getString(R.string.id_partido);
            cadena = cadena + " " + listaPartidos.get(i).getWvw_match_id();

//            try {
//                fechaComienzo = sdf2.parse(listaPartidos.get(i).getStart_time());
//                fechaFinal = sdf2.parse(listaPartidos.get(i).getEnd_time());
//                fechaActual = sdf2.parse(fechaActual.toString());
//            } catch (ParseException e){
//                e.getStackTrace();
//            }
//
//            cadena = cadena + " " + fechaComienzo.toString() + "-" + fechaFinal.toString();

            arrayList.add(cadena);
        }
        adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_expandable_list_item_1,
                arrayList);
        list.setAdapter(adapter);
    }*/

    /* ESTO ERA PARA EL RECYCLERVIEW, NO ES VIABLE HASTA QUE LO ARREGLEN
    public void rellenarDatos(List<Partido> listaPartidos){
        // Inicializar Partidos
        List items = new ArrayList<>();
        for (int i = 0; i < listaPartidos.size(); i++){
            items.add(listaPartidos.get(i));
        }

        // Obtener el Recycler
        recycler = (RecyclerView)findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new PartidoAdapter(items);
        recycler.setAdapter(adapter);
    }*/

    public long[] diferenciaEnDias (Date fechaMayor, Date fechaMenor){
        // Las 4 líneas posteriores se usaron para comprobar si se hacía la visualización
        // de la diferencia de horas, minutos y segundos.
        //Calendar cal = Calendar.getInstance(); // creates calendar
        //cal.setTime(new Date()); // sets calendar time/date
        //cal.add(Calendar.SECOND, 37); // adds one hour
        //fechaMayor = cal.getTime(); // returns new date object, one hour in the future
        long diferenciaEn_ms = fechaMayor.getTime() - fechaMenor.getTime();
        long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24);
        long[] valores = new long[2];
        long horas = 0;
        long minutos = 0;
        long segundos = 0;
        if (dias >= 1){
            valores[0] = 0;
            valores[1] = dias;
        } else if (dias < 1){
            horas = diferenciaEn_ms / (1000 * 60 * 60);
            minutos = diferenciaEn_ms / (1000 * 60);
            segundos = diferenciaEn_ms / (1000);
            if (horas <= 0){
                if (minutos <= 0){
                    valores[0] = 3;
                    valores[1] = segundos;
                } else {
                    valores[0] = 2;
                    valores[1] = minutos;
                }

            } else {
                valores[0] = 1;
                valores[1] = horas;
            }
        }
        return valores;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }
}
