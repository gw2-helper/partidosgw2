package es.uniovi.benja.partidosgw2;

/**
 * Created by cyrak on 10/3/15.
 */
public class Partido {
    // wvw_match_id -> String
    // red_world_id -> Int, long (Ejemplo:2013)
    // blue_world_id -> Int, long (Ejemplo: 2202)
    // green_world_id --> int, long
    // start_time --> date
    // end_time --> date
    protected String wvw_match_id;
    protected long red_world_id;
    protected long blue_world_id;
    protected long green_world_id;
    protected String start_time;
    protected String end_time;

    public Partido(){

    }

    public Partido(String wvw_match_ID, long red_world_ID, long blue_world_ID, long green_world_ID,
                   String start_TIME, String end_TIME){
        wvw_match_id = wvw_match_ID;
        red_world_id = red_world_ID;
        blue_world_id = blue_world_ID;
        green_world_id = green_world_ID;
        start_time = start_TIME;
        end_time = end_TIME;
    }

    public String getWvw_match_id() {
        return wvw_match_id;
    }

    public void setWvw_match_id(String wvw_match_id) {
        this.wvw_match_id = wvw_match_id;
    }

    public long getRed_world_id() {
        return red_world_id;
    }

    public void setRed_world_id(long red_world_id) {
        this.red_world_id = red_world_id;
    }

    public long getBlue_world_id() {
        return blue_world_id;
    }

    public void setBlue_world_id(long blue_world_id) {
        this.blue_world_id = blue_world_id;
    }

    public long getGreen_world_id() {
        return green_world_id;
    }

    public void setGreen_world_id(long green_world_id) {
        this.green_world_id = green_world_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}
